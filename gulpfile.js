'use strict';

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const gulp       = require('gulp'),
    plumber      = require('gulp-plumber'),
    sourcemaps   = require('gulp-sourcemaps'),
    rename       = require('gulp-rename'),
    pug          = require('gulp-pug'),
    sass         = require('gulp-sass'),
    htmlPrettify = require('gulp-html-prettify'),
    concat       = require('gulp-concat'),
    babel        = require('gulp-babel'),
    uglify       = require('gulp-uglify'),
    debug        = require('gulp-debug'),
    csso         = require('gulp-csso'),
    postcss      = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    mqpacker     = require('css-mqpacker'),
    pxtorem      = require('postcss-pxtorem'),
    gulpIf       = require('gulp-if'),
    notify       = require('gulp-notify');

const paths = {
    js:     './_dev/pages/front-page/modules/js/',
    libs:   './node_modules/',
    images: './_dev/pages/front-page/modules/images/',
    sass:   './_dev/pages/front-page/modules/scss/',
    pug:    './_dev/pages/front-page/modules/',
    dest:   {
        root: './'
    }
};

const sources = {
    jsSrc:       [
        paths.js + 'main.js'
    ],
    libsJsSrc:   [
        paths.libs + 'jquery/dist/jquery.min.js',
        paths.libs + 'swiper/dist/js/swiper.min.js'
    ],
    imgSrc:      paths.images + '**/*.{png,jpg,jpeg,gif,svg,ico}',
    sassSrc:     paths.sass + 'style.scss',
    libsSassSrc: [
        paths.libs + 'swiper/dist/css/swiper.min.css',
        paths.libs + 'normalize.css/normalize.css'
    ],
    pugSrc:      [paths.pug + '*.pug', '!' + paths.pug + '_*.pug'],
    pugTemplates: [paths.pug + 'template_parts/*.pug']

};

gulp.task('pug', function() {
    return gulp.src(sources.pugSrc)
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(htmlPrettify({indent_char: ' ', indent_size: 2}))
        .pipe(rename({extname: '.php'}))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.dest.root));

});

gulp.task('templatesPug', function() {
    return gulp.src(sources.pugTemplates)
        .pipe(plumber())
        .pipe(pug({pretty: true}))
        .pipe(htmlPrettify({indent_char: ' ', indent_size: 2}))
        .pipe(rename({extname: '.php'}))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.dest.root + 'template_parts'));

});

gulp.task('sass', function() {
    const AUTOPREFIXER_BROWSERS = [
        'last 2 versions',
        'ie >= 10'
    ];

    const POSTCSS_PLUGINS = [
        pxtorem({
            rootValue:         10,
            mediaQuery:        false,
            minPixelValue:     0,
            selectorBlackList: []
        }),
        mqpacker({sort: true}),
        autoprefixer({browsers: AUTOPREFIXER_BROWSERS})
    ];

    return gulp.src(sources.sassSrc)
        .pipe(debug())
        .pipe(plumber(notify.onError(function(err) {
            return {
                title:   'SCSS',
                message: err.message
            };
        })))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass({}))
        .pipe(debug({title: 'sass'}))
        .pipe(postcss(POSTCSS_PLUGINS))
        .pipe(debug({title: 'postcss'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulpIf(!isDevelopment, csso()))
        .pipe(debug({title: 'csso'}))
        .pipe(gulpIf(isDevelopment, sourcemaps.write('.')))
        .pipe(plumber.stop())
        .pipe(debug())
        .pipe(gulp.dest(paths.dest.root + 'assets/bundle/css'));
});

gulp.task('libsCss', function() {

    return gulp.src(sources.libsSassSrc)
        .pipe(plumber())
        .pipe(concat('libs.css'))
        .pipe(csso())
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.dest.root + 'assets/bundle/css'));
});

gulp.task('js', function() {
    return gulp.src(sources.jsSrc)
        .pipe(plumber())
        .pipe(debug({title: 'js'}))
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(babel({presets: ['env']}))
        .pipe(concat('scripts.js', {newLine: '\n\r'}))
        .pipe(gulpIf(isDevelopment, uglify()))
        .pipe(gulpIf(isDevelopment, sourcemaps.write('.')))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.dest.root + 'assets/bundle/js'));
});

gulp.task('libsJs', function() {
    return gulp.src(sources.libsJsSrc)
        .pipe(plumber())
        .pipe(concat('libs.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.dest.root + 'assets/bundle/js/'));
});

gulp.task('img', function() {
    return gulp.src(sources.imgSrc)
        .pipe(gulp.dest(paths.dest.root + 'assets/bundle/img'));
});

gulp.task('copy', function() {
    return gulp.src('./src/**/*.*')
        .pipe(gulp.dest(paths.dest.root));
});

gulp.task('watch', function() {
    gulp.watch(paths.pug + '**/*.pug', gulp.series(gulp.parallel('pug', 'templatesPug')));
    gulp.watch(paths.sass + '**/*.scss', gulp.series('sass'));
    gulp.watch(paths.js + '**/*.js', gulp.series('js'));
});

gulp.task('build', gulp.series(gulp.parallel('pug', 'templatesPug', 'sass', 'js', 'img', 'libsCss', 'libsJs')));

gulp.task('default', gulp.series('build', gulp.parallel('watch')));