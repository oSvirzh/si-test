$(document).ready(function() {
    let modelsSlider = new Swiper('.models-slider-js', {
        slidesPerView: 3,
        grabCursor: true,
        paginationClickable: true,
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.ms-pag-js',
            clickable: true,
        },
        navigation: {
            nextEl: '.ms-button-next-js',
            prevEl: '.ms-button-prev-jsZ',
        },
        breakpoints: {
            768: {
                slidesPerView: 2
            },
            544: {
                slidesPerView: 1
            }
        }
    })
})

function initMap() {
    var uluru = {lat: -25.363, lng: 131.044};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}